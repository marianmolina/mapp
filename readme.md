# Mapp

Mapp is a simple but addictive geography game. It gives you a country\* that you'll have to find on the map. Your goal ? Find all the countries to get all the map colored!

\*or territory not recognised as country by the United Nations.

## Features

- Register and login to your personal account to access your progress from any device.
- Update your profile and delete your account with its associated data.

- Full screen interactive map with zoom and pan features.

- Play mode : find a given country on the map, click on it to win it.
- Play mode : Get feedback according to your answer.
- Play mode : support dataset bugs, some countries are not selectable, the user can click on a neighboring country and validate himself the result.
- Learn mode : get countries names by hovering over them, hide the quizz section.

- Choose a custom color for the countries you got right.
- Reset the game progress.

## Thanks / Notes

- Articles that helped me to setup the backend :
  https://dev.to/salarc123/mern-stack-authentication-tutorial-part-1-the-backend-1c57
  https://www.positronx.io/react-mern-stack-crud-app-tutorial/

- Fireship about express middleware : "Express doesn't parse json in the body by default. (...) we need to (...) setup a middleware that tells express to parse json before the actual data hits the function that we are using here to handle the request." https://www.youtube.com/watch?v=-MTSQjw5DrM

- Special thanks to Curran Kelleher for the free and excellent quality teaching of d3.js : https://www.youtube.com/watch?v=30lR5BlcO48&list=PL9yYRbwpkykuK6LSMLH3bAaPpXaDUXcLV

## Licence

This work is licensed under a Creative Commons Attribution 4.0 International License.
http://creativecommons.org/licenses/by/4.0/
