import { useRef } from "react";

const ColorPicker = ({ userColor, setUserColor }) => {
  const inputRef = useRef(null);
  return (
    <button
      onClick={() => {
        inputRef.current.click();
      }}
      className="mx-7 hover:cursor-pointer flex items-center  fill-[#333333] hover:fill-[#181818]"
    >
      <div
        className="w-5 h-5 rounded-full shadow-md hover:cursor-pointer"
        style={{ backgroundColor: `${userColor}` }}
      >
        <input
          ref={inputRef}
          type="color"
          value={userColor}
          onChange={(e) => {
            setUserColor(e.target.value);
          }}
          className="invisible"
        />
      </div>
      <p className="text-xs ml-2 text-[#181818]">Color</p>
    </button>
  );
};

export default ColorPicker;
