import { useRef } from "react"

import { geoPath, select, zoom } from "d3"
import { geoCylindricalStereographic } from "d3-geo-projection"

const WIDTH = window.innerWidth
const HEIGHT = window.innerHeight

const projection = geoCylindricalStereographic()
	.scale(WIDTH / HEIGHT > 2.07 ? HEIGHT / 2.5 : HEIGHT / 3)
	.translate([WIDTH / 2, HEIGHT / 2])

const path = geoPath(projection)
// const graticule = geoGraticule();

const Map = ({
	data: { countries, interiors },
	countryToGuess,
	display,
	selectedCountry,
	setSelectedCountry,
	countriesWon,
	userColor,
	play,
}) => {
	const svgRef = useRef(null)

	// ZOOM and PAN
	const zoomed = ({ transform }) => {
		g.attr("transform", transform)
	}

	const svg = select("#map")
	const g = select("g")

	svg.call(
		zoom()
			.extent([
				[0, 0],
				[WIDTH, HEIGHT],
			])
			.scaleExtent([1, 9])
			.on("zoom", zoomed),
	)

	return (
		<>
			<svg id="map" width={WIDTH} height={HEIGHT} className=" bg-[#0e4f6b]">
				<g ref={svgRef}>
					<g>
						<path d={path({ type: "Sphere" })} className="fill-[#0e4f6b] " />

						{countries.features.map((feature) => (
							<path
								key={Math.random()}
								id={feature.properties.name}
								d={path(feature)}
								style={
									countriesWon.includes(feature.properties.name)
										? { fill: userColor, stroke: "none" }
										: null
								}
								className={`${
									countriesWon.includes(feature.properties.name)
										? `hover:fill-transparent hover:cursor-auto`
										: feature.properties.name === selectedCountry && play
										? "fill-[#435e28] hover:fill-[#435e28] "
										: "fill-[#8cb460] hover:fill-[#628340]"
								} ${
									feature.properties.name === countryToGuess && display && play
										? "fill-[#ffd900] hover:fill-[#ffd900] stroke-[#ffd900]"
										: null
								} stroke-none hover:cursor-pointer`}
								strokeWidth="3"
								onClick={(e) => {
									if (!display) {
										setSelectedCountry(feature.properties.name)
									}
								}}
							>
								{display || !play ? <title>{feature.properties.name}</title> : null}
							</path>
						))}

						<path
							d={path(interiors)}
							className="fill-transparent stroke-[#597937] st"
							strokeWidth="0.25"
						/>
					</g>
				</g>
			</svg>
		</>
	)
}

export default Map
