import { useEffect, useState } from "react"

const Questions = ({
	countryToGuess,
	setCountryToGuess,
	quizzStatus,
	setQuizzStatus,
	display,
	setDisplay,
	selectedCountry,
	setSelectedCountry,
	countriesWon,
	setCountriesWon,
	countryBug,
	setCountryBug,
	countriesBug,
	requestStatus,
	setRequestStatus,
	play,
	countriesLeft,
	setCountriesLeft,
}) => {
	const [neighbours, setNeighbours] = useState([])

	// RESET DATA (only after selectable countries i.e. without bug)
	const resetData = () => {
		setCountryToGuess("...")
		setQuizzStatus(null)
		setSelectedCountry(null)
		setCountryBug(false)
		setRequestStatus(false)
		setNeighbours([])
		setDisplay(false)
	}

	// UPDATE COUNTRIES LEFT TO GUESS
	useEffect(() => {
		setCountriesLeft((countriesLeft) =>
			countriesLeft.filter((country) => !countriesWon.includes(country)),
		)
	}, [countriesWon])

	// SET A NEW COUNTRY TO GUESS
	useEffect(() => {
		if (!display) {
			const index = Math.floor(Math.random() * (0, countriesLeft.length))
			setCountryToGuess(countriesLeft[index])
		}
	}, [display])

	// CHECK IF THE COUNTRY TO GUESS HAS A BUG
	useEffect(() => {
		if (countriesBug.map((d) => d.country).includes(countryToGuess)) {
			// Array of neighbouring countries
			for (let item in countriesBug) {
				if (countriesBug[item].country === countryToGuess) {
					setNeighbours(countriesBug[item].neighbours)
				}
			}
			setCountryBug(true)
		}
	}, [countryToGuess])

	// CHECK IF USER WON OR LOSE
	useEffect(() => {
		if (selectedCountry) {
			setDisplay(true)

			if (selectedCountry === countryToGuess) {
				setQuizzStatus("Win")
			} else if (neighbours && neighbours.includes(selectedCountry)) {
				setRequestStatus(true)
			} else {
				setQuizzStatus("Lose")
			}
		}
	}, [selectedCountry])

	// ADD WON COUNTRY TO LIST OF WON COUNTRIES
	useEffect(() => {
		if (quizzStatus === "Win") {
			setCountriesWon((countries) => [...countries, countryToGuess])
			setQuizzStatus("Pending")
		}
	}, [quizzStatus])

	return (
		<div>
			{/* END OF THE GAME */}
			{countriesLeft.length === 0 ? (
				<div className="flex absolute top-0 left-0 justify-center items-center w-screen h-screen bg-black/75">
					<p className="transition ease-in-out delay-150 duration-300 scale-110 h-24 text-4xl font-semibold text-transparent text-white bg-clip-text bg-gradient-to-r from-[#d3d48d] via-[#d4991a] to-[#d3d48d]">
						Congratulations, you found all the countries on the map ! 🥳
					</p>
				</div>
			) : (
				<div
					className={`${
						!play ? "translate-y-24" : "translate-y-0"
					} delay-75 transition-transform ease-in flex fixed right-0 left-0 bottom-8 justify-center items-center p-4 px-6 mx-auto w-2/3 h-16 rounded-md shadow-md bg-white/75`}
				>
					<div className="mr-2 text-lg text-center">
						{/* ASK QUESTION */}
						{!display && countriesLeft.length > 0 ? (
							<div className="flex justify-center">
								<p>
									Where is <span className="font-bold">{countryToGuess}</span> ?
								</p>
								{countryBug ? (
									<div className="flex items-center">
										<svg
											width="20"
											height="20"
											viewBox="0 0 127 126"
											xmlns="http://www.w3.org/2000/svg"
											className="ml-4 fill-[#9b0d0d]"
										>
											<path
												d="M63.2481 0.0979919C28.8203 0.0979919 0.810059 28.24 0.810059 62.8309C0.810059 97.4218 28.952 125.564 63.543 125.564C98.1339 125.564 126.276 97.4218 126.276 62.8309C126.276 28.24 98.0021 0.0979919 63.2481 0.0979919ZM63.543 113.017C35.8715 113.017 13.3566 90.5024 13.3566 62.8309C13.3566 35.1594 35.7335 12.6446 63.2481 12.6446C91.089 12.6446 113.729 35.1594 113.729 62.8309C113.729 90.5024 91.2144 113.017 63.543 113.017Z"
												fill="inherit"
											/>
											<path
												d="M57.2695 31.4644H69.8161V75.3775H57.2695V31.4644ZM57.2695 81.6508H69.8161V94.1973H57.2695V81.6508Z"
												fill="inherit"
											/>
										</svg>
										<p className={` text-sm ml-2 text-[#611d1d]`}>
											This country can't be clicked. Click on a neighbouring country instead.
										</p>
									</div>
								) : null}
							</div>
						) : null}

						{/* USER VERIFICATION IF COUNTRY BUG */}
						{display && requestStatus ? (
							<div className="flex items-center">
								<p>
									Did you get <span className=" bg-[#ffd900]">{countryToGuess}</span> right?
								</p>
								<button
									title="Confirm"
									className="shadow px-8 text-center py-1 mr-2 ml-8  rounded bg-[#ffffff] hover:bg-[#e6e5e5]"
									onClick={() => {
										setQuizzStatus("Win")
										setRequestStatus(false)
										setDisplay(false)
										setCountryBug(false)
										setNeighbours([])
										setSelectedCountry(null)
									}}
									autoFocus
								>
									Yes
								</button>
								<button
									title="Deny"
									className="shadow px-8 py-1 ml-2  rounded bg-[#ffffff] hover:bg-[#e6e5e5]"
									onClick={() => {
										setQuizzStatus("Lose")
										setRequestStatus(false)
										setDisplay(false)
										setCountryBug(false)
										setNeighbours([])
										setSelectedCountry(null)
									}}
								>
									No
								</button>
							</div>
						) : null}

						{/* USER ANSWERED WRONG OR RIGHT */}
						{display && quizzStatus === "Lose" && !requestStatus ? (
							<div className="flex justify-center items-center align-middle">
								<p>
									No, this is <span className="font-bold bg-[#628340] ">{selectedCountry}</span>.
									Try to find
									<span className="font-bold bg-[#ffd900] mx-1">{countryToGuess}</span>
									on the map
								</p>
							</div>
						) : display && quizzStatus !== "Lose" && !requestStatus ? (
							<p className="font-bold text-center">Well Done!</p>
						) : null}
					</div>

					{/* NEXT BUTTON */}
					{display && quizzStatus && !requestStatus ? (
						<button
							title="Next"
							onClick={resetData}
							className="absolute right-4 rounded-md bg-white hover:bg-slate-100 p-2  hover:fill-[#181818] fill-[#333333] "
							autoFocus
						>
							Next country
						</button>
					) : null}
				</div>
			)}
		</div>
	)
}

export default Questions
