export const countriesBug = [
  // AFRICA
  {
    country: "eSwatini",
    neighbours: ["South Africa", "Mozambique"],
  },
  {
    country: "Lesotho",
    neighbours: ["South Africa"],
  },
  {
    country: "Niger",
    neighbours: [
      "Nigeria",
      "Chad",
      "Libya",
      "Algeria",
      "Mali",
      "Burkina Faso",
      "Benin",
    ],
  },
  {
    country: "S. Sudan",
    neighbours: [
      "Sudan",
      "Ethiopia",
      "Dem. Rep. Congo",
      "Central African Rep.",
      "Kenya",
    ],
  },
  {
    country: "Togo",
    neighbours: ["Ghana", "Benin", "Burkina Faso"],
  },

  {
    country: "Uganda",
    neighbours: ["Tanzania", "Rwanda", "Dem. Rep. Congo", "Kenya"],
  },
  {
    country: "Zambia",
    neighbours: [
      "Mozambique",
      "Angola",
      "Malawi",
      "Botswana",
      "Dem. Rep. Congo",
      "Tanzania",
    ],
  },
  {
    country: "Zimbabwe",
    neighbours: ["Mozambique", "South Africa", "Botswana", "Zambie"],
  },
  // SOUTH AMERICA
  {
    country: "Paraguay",
    neighbours: ["Brazil", "Bolivia", "Argentina"],
  },
  {
    country: "Venezuela",
    neighbours: ["Colombia", "Guyana"],
  },
  // ASIA
  {
    country: "Macao",
    neighbours: ["China"],
  },
  {
    country: "Nepal",
    neighbours: ["India", "China"],
  },
  {
    country: "Pakistan",
    neighbours: ["India", "Afghanistan", "China", "Iran"],
  },
  {
    country: "Uzbekistan",
    neighbours: [
      "Kazakhstan",
      "Turkmenistan",
      "Tidjikistan",
      "Tadjikistan",
      "Afghanistan",
    ],
  },
  // Europe
  {
    country: "Andorra",
    neighbours: ["France", "Spain"],
  },
  {
    country: "Luxembourg",
    neighbours: ["France", "Belgium", "Germany"],
  },

  {
    country: "Macedonia",
    neighbours: ["Greece", "Bulgaria", "Kosovo", "Albania"],
  },
  {
    country: "Monaco",
    neighbours: ["Italy", "France"],
  },
  {
    country: "San Marino",
    neighbours: ["Italy"],
  },
  {
    country: "Serbia",
    neighbours: [
      "Romania",
      "Bulgaria",
      "Kosovo",
      "Montenegro",
      "Hungary",
      "Macedonia",
      "Croatia",
      "Bosnia and Herz.",
    ],
  },
  {
    country: "Slovakia",
    neighbours: ["Hungary", "Poland", "Ukrainia", "Austria", "Czechia"],
  },
  {
    country: "Slovenia",
    neighbours: ["Italy", "Hungary", "Croatia", "Austria"],
  },
  {
    country: "Switzerland",
    neighbours: ["Austria", "France", "Germany", "Italy", "Liechtenstein"],
  },
  {
    country: "Vatican",
    neighbours: ["Italy"],
  },
];
