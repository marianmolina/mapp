import { useState, useEffect } from "react"
import { json } from "d3"
import { feature, mesh } from "topojson-client" // Convert topojson data to geojson data

const jsonUrl = "data/countries-50m.json" //https://unpkg.com/world-atlas@2.0.2/countries-50m.json"

// const jsonUrl = "data/ne_10m_admin_0_countries.json" //https://unpkg.com/world-atlas@2.0.2/countries-50m.json"

export const useData = () => {
	const [data, setData] = useState(null)

	useEffect(() => {
		json(jsonUrl).then((topology) => {
			console.log(topology.objects)
			const { countries } = topology.objects
			setData({
				countries: feature(topology, countries),
				interiors: mesh(topology, countries, (a, b) => a !== b),
			})
		})
	}, [])

	return data
}
