import ReactDOM from "react-dom/client"
import { StrictMode } from "react"

import Home from "./pages/home"

import "./index.css"

ReactDOM.createRoot(document.getElementById("root")).render(
	<StrictMode>
		<Home />
	</StrictMode>,
)
