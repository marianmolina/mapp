import { useState, useEffect, useRef } from "react"

import { useData } from "../hooks/useData"
import { countriesBug } from "../constants/countriesBug"

import Header from "../components/Header"
import Map from "../components/Map"
import Questions from "../components/Questions"

if (!localStorage.getItem("mapp-color")) {
	localStorage.setItem("mapp-color", "#16A185")
}
if (!localStorage.getItem("mapp-countries")) {
	localStorage.setItem("mapp-countries", "")
}

const Home = () => {
	const mapData = useData()
	const [countryToGuess, setCountryToGuess] = useState("...")
	const [selectedCountry, setSelectedCountry] = useState(null)
	const [countriesWon, setCountriesWon] = useState(() => localStorage.getItem("mapp-countries"))
	const [quizzStatus, setQuizzStatus] = useState(null) // Lose or Win
	const [display, setDisplay] = useState(false) // Display question feedback
	const [userColor, setUserColor] = useState(() => localStorage.getItem("mapp-color")) // For won country
	const [play, setPlay] = useState(true) // False = learn mode
	const [countryBug, setCountryBug] = useState(false) // Country to guess is not selectable
	const [requestStatus, setRequestStatus] = useState(false) // Ask user if he was correct on countryBug

	const [countriesLeft, setCountriesLeft] = useState(null)

	useEffect(() => {
		mapData
			? setCountriesLeft(
					mapData.countries.features
						.map((feature) => feature.properties.name)
						.filter((country) => !countriesWon.includes(country)),
			  )
			: null
	}, [mapData])

	const componentRef = useRef(null)

	// UPDATE USER MAP DATA
	useEffect(() => {
		if (componentRef.current) {
			localStorage.setItem("mapp-countries", countriesWon)
		}
	}, [countriesWon])

	useEffect(() => {
		if (componentRef.current) {
			localStorage.setItem("mapp-color", userColor)
		}
	}, [userColor])

	if (!mapData || countriesLeft === null) {
		return (
			<div className="bg-gradient-to-r from-[#8CB460] to-[#0e4f6b] h-screen flex justify-center items-center">
				<div className="animate-spin border-[10px] border-white/30 rounded-full border-t-[10px] border-t-white w-24 h-24"></div>
			</div>
		)
	}

	return (
		<div ref={componentRef} className="flex w-screen h-screen bg-[#0e4f6b]">
			<div className="flex absolute w-[475px] ">
				<Header
					userColor={userColor}
					setUserColor={setUserColor}
					play={play}
					setPlay={setPlay}
					setCountriesWon={setCountriesWon}
				/>
			</div>
			<Map
				data={mapData}
				countryToGuess={countryToGuess}
				display={display}
				setDisplay={setDisplay}
				selectedCountry={selectedCountry}
				setSelectedCountry={setSelectedCountry}
				countriesWon={countriesWon}
				userColor={userColor}
				play={play}
			/>

			<Questions
				countryToGuess={countryToGuess}
				setCountryToGuess={setCountryToGuess}
				quizzStatus={quizzStatus}
				setQuizzStatus={setQuizzStatus}
				display={display}
				setDisplay={setDisplay}
				selectedCountry={selectedCountry}
				setSelectedCountry={setSelectedCountry}
				countriesWon={countriesWon}
				setCountriesWon={setCountriesWon}
				countryBug={countryBug}
				setCountryBug={setCountryBug}
				countriesBug={countriesBug}
				requestStatus={requestStatus}
				setRequestStatus={setRequestStatus}
				play={play}
				countriesLeft={countriesLeft}
				setCountriesLeft={setCountriesLeft}
			/>
		</div>
	)
}

export default Home
