module.exports = {
  content: ["./index.html", "./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      screens: {
        xxs: "400px",
        xs: "540px",
      },
    },
  },
  plugins: [],
};
